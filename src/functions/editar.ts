import { DynamoDBDocument, UpdateCommand } from "@aws-sdk/lib-dynamodb";
import { DynamoDB } from "@aws-sdk/client-dynamodb";
import { UpdateCommandInput } from "@aws-sdk/lib-dynamodb";

const { TABLE_NAME } = process.env;
const RESERVED_RESPONSE = `Error: está utilizando palabras clave reservadas de AWS como atributos`,
  DYNAMODB_EXECUTION_ERROR = `Error: La ejecución de DynamoDB causó un error, por favor revise los Logs de CloudWatch.`;
const db = DynamoDBDocument.from(new DynamoDB({}));

export const editar = async (event: any = {}) => {
  if (!event.body) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el Body de la petición",
    };
  }
  const item =
    typeof event.body == "object" ? event.body : JSON.parse(event.body);

  if (!event.queryStringParameters.id) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el id que es requerido",
    };
  }

  const GetParams = {
    TableName: TABLE_NAME,
    Key: {
      id: event.queryStringParameters.id,
    },
  };
  const response = (await db.get(GetParams)).Item;

  if (!item.nombre) {
    item.nombre = response?.nombre || item.nombre;
  }

  if (!item.apellido) {
    item.apellido = response?.apellido || item.apellido;
  }

  if (!item.email) {
    item.email = response?.email || item.email;
  }

  item.id = event.queryStringParameters.id;

  if (!item.telefono) {
    item.telefono = response?.telefono || item.telefono;
  }

  console.log(item);

  const params : UpdateCommandInput = ({
    TableName: TABLE_NAME,
    Key: {
      id: event.queryStringParameters.id
    },
    UpdateExpression: "set nombre = :nombre, apellido = :apellido, email = :email, telefono = :telefono",
    ExpressionAttributeValues: {
      ":nombre": item.nombre,
      ":apellido": item.apellido,
      ":email": item.email,
      ":telefono": item.telefono,
    },
    ReturnValues: "ALL_NEW",
  });

  try {
    db.update(params);
    return { statusCode: 200, body: "Elemento editado" };
  } catch (dbError: any) {
    const errorResponse =
      dbError.code === "ValidationException" &&
      dbError.message.includes("reserved keyword")
        ? RESERVED_RESPONSE
        : DYNAMODB_EXECUTION_ERROR;
    const jsonResp = JSON.stringify({
      statusCode: 500,
      error: errorResponse,
      causa: dbError,
    });
    return { statusCode: 500, body: jsonResp };
  }
};
