import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { DynamoDB } from "@aws-sdk/client-dynamodb";
import { usuario } from "../interfaces/usuario.interface";

const { TABLE_NAME } = process.env;
const RESERVED_RESPONSE = `Error: está utilizando palabras clave reservadas de AWS como atributos`,
  DYNAMODB_EXECUTION_ERROR = `Error: La ejecución de DynamoDB causó un error, por favor revise los Logs de CloudWatch.`;
const db = DynamoDBDocument.from(new DynamoDB({}));

export const ingresar = async (event: any = {}) => {
  if (!event.body) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el Body de la petición",
    };
  }
  const item =
    typeof event.body == "object" ? event.body : JSON.parse(event.body);

  if (!item.id) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el id que es requerido",
    };
  }

  if (!item.nombre) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el nombre que es requerido",
    };
  }

  if (!item.apellido) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el apellido que es requerido",
    };
  }

  if (!item.email) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el email que es requerido",
    };
  }

  if (!item.telefono) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el telefono que es requerido",
    };
  }

  const itemMapeado: usuario = {
    id: item.id,
    nombre: item.nombre,
    apellido: item.apellido,
    email: item.email,
    telefono: item.telefono,
  };

  const params = {
    TableName: TABLE_NAME,
    Item: itemMapeado,
  };

  try {
    await db.put(params);
    return { statusCode: 201, body: "Elemento subido" };
  } catch (dbError: any) {
    const errorResponse =
      dbError.code === "ValidationException" &&
      dbError.message.includes("reserved keyword")
        ? RESERVED_RESPONSE
        : DYNAMODB_EXECUTION_ERROR;
    const jsonResp = JSON.stringify({
      statusCode: 500,
      error: errorResponse,
      causa: dbError,
    });
    return { statusCode: 500, body: jsonResp };
  }
};
