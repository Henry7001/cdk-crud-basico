import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { DynamoDB } from "@aws-sdk/client-dynamodb";

const { TABLE_NAME } = process.env;
const RESERVED_RESPONSE = `Error: está utilizando palabras clave reservadas de AWS como atributos`,
  DYNAMODB_EXECUTION_ERROR = `Error: La ejecución de DynamoDB causó un error, por favor revise los Logs de CloudWatch.`;
const db = DynamoDBDocument.from(new DynamoDB({}));

export const obtener = async (event: any = {}) => {
  try {
    console.log("Received event:", JSON.stringify(event, null, 2));
    if (event.queryStringParameters && event.queryStringParameters.id) {
      const params = {
        TableName: TABLE_NAME,
        Key: {
          id: event.queryStringParameters.id,
        }
      };
      const response = await db.get(params);
      return { statusCode: 200, body: JSON.stringify(response.Item) };
    }
    console.log("No id provided, returning all data");
    const response = await db.scan({ TableName: TABLE_NAME });
    return { statusCode: 200, body: JSON.stringify(response.Items) };
  } catch (dbError: any) {
    console.error(dbError);
    const errorResponse =
      dbError.code === "ValidationException" &&
      dbError.message.includes("reserved keyword")
        ? RESERVED_RESPONSE
        : DYNAMODB_EXECUTION_ERROR;
    const jsonResp = JSON.stringify({
      statusCode: 500,
      error: errorResponse,
      causa: dbError,
    });
    return { statusCode: 500, body: jsonResp };
  }
};
