import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { DynamoDB } from "@aws-sdk/client-dynamodb";

const { TABLE_NAME } = process.env;
const RESERVED_RESPONSE = `Error: está utilizando palabras clave reservadas de AWS como atributos`,
  DYNAMODB_EXECUTION_ERROR = `Error: La ejecución de DynamoDB causó un error, por favor revise los Logs de CloudWatch.`;
const db = DynamoDBDocument.from(new DynamoDB({}));

export const eliminar = async (event: any = {}) => {
  if (!event.queryStringParameters.id) {
    return {
      statusCode: 400,
      body: "Petición erronea, no se encuentra el id que es requerido.",
    };
  }
  const params = {
    TableName: TABLE_NAME,
    Key: {
      id: event.queryStringParameters.id,
    },
  };
  try {
    await db.delete(params);
    return { statusCode: 200, body: "Elemento eliminado" };
  } catch (dbError: any) {
    const errorResponse =
      dbError.code === "ValidationException" &&
      dbError.message.includes("reserved keyword")
        ? RESERVED_RESPONSE
        : DYNAMODB_EXECUTION_ERROR;
    const jsonResp = JSON.stringify({
      statusCode: 500,
      error: errorResponse,
      causa: dbError,
    });
    return { statusCode: 500, body: jsonResp };
  }
};
