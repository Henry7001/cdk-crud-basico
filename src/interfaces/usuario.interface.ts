export interface usuario {
    id: string;
    nombre: string;
    apellido: string;
    email: string;
    telefono: string;
}