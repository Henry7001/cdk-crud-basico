import * as lambda from "aws-cdk-lib/aws-lambda";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import * as cdk from "aws-cdk-lib";
import * as path from "path";

export class CustomLambda {
  private stack: cdk.Stack;
  private stackName: string;

  constructor(stack: cdk.Stack, stackName: string) {
    this.stack = stack;
    stackName = stackName;
  }

  getFunction(
    handlerFn: string,
    pathHandler: string,
    tableName: string
  ): lambda.Function {
    return new NodejsFunction(this.stack, handlerFn + "Fn", {
      functionName: `${this.stackName}-${handlerFn}`,
      runtime: lambda.Runtime.NODEJS_20_X,
      handler: handlerFn,
      environment: {
        TABLE_NAME: tableName,
      },
      entry: path.join(__dirname, "../..", pathHandler)
    });
  }
}
