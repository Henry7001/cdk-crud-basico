import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as dynamoDB from 'aws-cdk-lib/aws-dynamodb';
import { CustomLambda } from "./utils/CustomLambda";

export class CrudCdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const lambdaGenerator = new CustomLambda(this, "CrudCdkStack");

    const table = new dynamoDB.Table(this, 'Usuarios', {
      partitionKey: { name: 'id', type: dynamoDB.AttributeType.STRING },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    const obtener = lambdaGenerator.getFunction(
      "obtener",
      "src/functions/obtener.ts",
      table.tableName
    );

    const ingresar = lambdaGenerator.getFunction(
      "ingresar",
      "src/functions/ingresar.ts",
      table.tableName
    );

    const editar = lambdaGenerator.getFunction(
      "editar",
      "src/functions/editar.ts",
      table.tableName
    );

    const eliminar = lambdaGenerator.getFunction(
      "eliminar",
      "src/functions/eliminar.ts",
      table.tableName
    );

    table.grantReadWriteData(obtener);
    table.grantReadWriteData(ingresar);
    table.grantReadWriteData(editar);
    table.grantReadWriteData(eliminar);

    const APIGW=new cdk.aws_apigateway.RestApi(this, 'CrudApi', {
      defaultCorsPreflightOptions: {
        allowOrigins: ["*"],
        allowMethods: ["*"],
        allowHeaders: ["*"],
      },
      deployOptions: {
        stageName: 'v1',
      },
    });
    APIGW.root.addResource('obtener').addMethod('GET', new cdk.aws_apigateway.LambdaIntegration(obtener));
    APIGW.root.addResource('ingresar').addMethod('POST', new cdk.aws_apigateway.LambdaIntegration(ingresar));
    APIGW.root.addResource('editar').addMethod('PUT', new cdk.aws_apigateway.LambdaIntegration(editar));
    APIGW.root.addResource('eliminar').addMethod('DELETE', new cdk.aws_apigateway.LambdaIntegration(eliminar));

  }
}
